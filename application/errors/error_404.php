<?php
$CI =& get_instance();
$IDIOMA = strtolower($CI->IDIOMA);
$Imagem = ($IDIOMA=='en') ? '404_en.png' : '404_es.png' ;
$Imagem = base_url("assets/site/img/{$Imagem}");
$Link = site_url();
?><!DOCTYPE html>
<html lang="<?php echo $IDIOMA; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ERROR 404</title>
<style>
body {
    background-image: url('<?php echo $Imagem ?>');
	background-repeat: no-repeat;
	background-position: center top;
	min-height: 600px;
 	min-width::800px;
}
a {
	width: 800px;
	height: 600px;
	margin: 0px auto !important;
	position:relative;
	display: block;
	background: none;
	text-indent: -99999px;
	overflow: hidden;
	cursor: pointer;
	border:none;
	text-decoration:none;
	font-style:normal;
}
</style>
</head>

<body>
<a href="<?php echo $Link; ?>"><?php echo $Link; ?></a> 
</body>
</html>
